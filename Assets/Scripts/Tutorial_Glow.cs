﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Tutorial_Glow : MonoBehaviour 
{

	public float Speed;

	Image image;
	
	void Start ()
	{
		image = GetComponent<Image> ();
	}

	void Update ()
	{
		image.color = new Color(1, 1, 1, Mathf.Abs((Mathf.Sin(Time.realtimeSinceStartup*Speed)+0.2f)/1.2f));
	}
}
