﻿using UnityEngine;
using System.Collections;

public class Candle : MonoBehaviour {

	public Sprite[] ExtinguishedSprite;

	float _timeNeededToLit = 1f; // TODO Move to BalanceManager
	public float TimeNeededToLit {
		get {return _timeNeededToLit;}
		set {_timeNeededToLit = value;}
	}
	public bool Lit {
		get;
		set;
	}

	float _timeNeededToExtinguish;

	Monument MonumentParent;
	SpriteRenderer _sr;
	float _litTime;
	float _extinguishTime;
	bool _lightCandleProcess;
	bool _extinguishCandleProcess;
	int _candleIndex;

	ParticleSystem fire;

	public float LitProgress { get { return _litTime / TimeNeededToLit; } }

	void Awake () {
		_candleIndex = Random.Range (0, ExtinguishedSprite.Length);
		_sr = GetComponent<SpriteRenderer>();
		MonumentParent = transform.GetComponentInParent<Monument>();
		InitFireParticleSystem (_candleIndex);
	}

	void InitFireParticleSystem (int candleIndex)
	{
		fire = GetComponentInChildren<ParticleSystem> ();
		switch(candleIndex)
		{
		case 0:
			fire.transform.localPosition = new Vector3(0.05f, 0.46f, 0f);
			break;
		case 1:
			fire.transform.localPosition = new Vector3(0f, 0.75f, 0f);
			break;
		case 2:
			fire.transform.localPosition = new Vector3(-0.02f, 0.45f, 0f);
			break;
		case 3:
			fire.transform.localPosition = new Vector3(0.03f, 0.63f, 0f);
			break;
		}
	}

	void Start () {
		_sr.sprite = ExtinguishedSprite[_candleIndex];
		if (Random.Range(0, 2) == 1)
		{
			_sr.transform.localScale = new Vector3(-_sr.transform.localScale.x, _sr.transform.localScale.y, 0f);
			Transform progressTransform = gameObject.GetComponentInChildren<CandleProgress>().transform;
			progressTransform.localScale = new Vector3(-progressTransform.localScale.x, progressTransform.localScale.y, 0f);
		}
		PolygonCollider2D col = gameObject.AddComponent<PolygonCollider2D>();
		col.isTrigger = true;
		fire.Stop();
	}

	bool IsPossibleToLit ()
	{
		return !Lit && MonumentParent.Active;
	}

	void ToggleLit()
	{
		if (IsPossibleToLit () || Lit)
		{
			Lit = !Lit;
			MonumentParent.CandleUpdate(Lit);
			if(Lit) 
			{
				fire.Play();
				GetComponent<AudioSource>().Play ();
			} 
			else
			{
				fire.Stop();
			}
		}
	}

	void Update () {
		if (_lightCandleProcess)
		{
			LightCandle();
		}
		if (_extinguishCandleProcess)
		{
			ExtinguishCandle();
		}
	}
	
	void LightCandle ()
	{
		if (!GameManager.Instance.IsPlaying)
			return;

		Monk.Instance.Lighting = true;
		_litTime += Time.deltaTime;
		if (_litTime >= TimeNeededToLit) {
			ToggleLit ();
			Monk.Instance.Lighting = false;
			_litTime = 0f;
			_lightCandleProcess = false;
		}
		if (!MonumentParent.Active)
		{
			Monk.Instance.Lighting = false;
			_litTime = 0f;
			_lightCandleProcess = false;
		}
	}

	void ExtinguishCandle ()
	{
		if (GameManager.Instance.Won)
			return;

		_extinguishTime += Time.deltaTime;
		if (_extinguishTime >= _timeNeededToExtinguish) {
			ToggleLit ();
			_extinguishTime = 0f;
			_extinguishCandleProcess = false;
		}
	}

	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.tag == "CandleHand"
		    && !Lit 
		    && MonumentParent.Active) 
		{
			_lightCandleProcess = true;
		} 
		else if (other.gameObject.GetComponent<Ghost> () != null 
		         && Lit)
		{ 
			_timeNeededToExtinguish = other.gameObject.GetComponent<Ghost>().TimeAboveCandle;
			_extinguishCandleProcess = true;
		}
	}

	void OnTriggerStay2D(Collider2D other)
	{
		if (other.tag == "CandleHand" 
		    && !Lit 
		    && MonumentParent.Active)
		{
			_lightCandleProcess = true;
		}
	}

	void OnTriggerExit2D(Collider2D other)
	{
		if (other.tag == "CandleHand")
		{
			Monk.Instance.Lighting = false;
			_litTime = 0f;
			_lightCandleProcess = false;
		}
		else if (other.gameObject.GetComponent<Ghost> () != null)
		{
			_extinguishTime = 0f;
			_extinguishCandleProcess = false;
		}
	}
}
