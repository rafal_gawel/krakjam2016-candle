﻿using UnityEngine;
using System.Collections;

public class PierunAnimation : MonoBehaviour {

	void Start () {
//		ShowPierun();
	}

	public void ShowPierun(Sprite pierunSprite)
	{
		GetComponent<SpriteRenderer>().sprite = pierunSprite;
		LeanTween.alpha(gameObject, 1f, 0.2f).setOnComplete(() => 
		{
			LeanTween.alpha(gameObject, 0f, 0.2f).setOnComplete( ()=>
			{
				GetComponent<SpriteRenderer>().enabled = false;
			});
		});
	}
}
