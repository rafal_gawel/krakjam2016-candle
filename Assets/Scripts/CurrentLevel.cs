﻿using UnityEngine;
using System.Collections;

public class CurrentLevel
{

	static int currentLevel = 0;
	const int MAX_LEVELS = 4;

	public static void LoadNextLevel()
	{
		currentLevel++;
		if(currentLevel > MAX_LEVELS)
		{
			currentLevel = 0;
			Application.LoadLevel("Congratulations");
			return;
		}
		Application.LoadLevel ("Level" + currentLevel.ToString ());
		Debug.Log ("Level "+currentLevel.ToString());
	}

}
