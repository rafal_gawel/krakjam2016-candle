﻿using UnityEngine;
using System.Collections;

public class GhostMaleAnimation : MonoBehaviour {

	public Transform LeftHand;
	public Transform RightHand;

	float _leftHandStartRot;
	float _leftHandChange;
	float _rightHandStartRot;
	float _rightHandChange;

	const int animAnglesDegree = 25;

	void Start () {
		_leftHandStartRot = -Quaternion.Angle(Quaternion.identity, LeftHand.localRotation); // It's stupid but it works
		_rightHandStartRot = Quaternion.Angle(Quaternion.identity, RightHand.localRotation); // It's stupid but it works
//		Debug.Log ("_leftHandStartRot" + _leftHandStartRot, gameObject);
//		Debug.Log ("_rightHandStartRot" + _rightHandStartRot, gameObject);
	}
	
	void Update () {
		_leftHandChange = animAnglesDegree * Mathf.Sin (2 * Time.time);
		LeftHand.localRotation = Quaternion.Euler(new Vector3(LeftHand.localRotation.x, LeftHand.localRotation.y, _leftHandStartRot + _leftHandChange));

		_rightHandChange = animAnglesDegree * Mathf.Sin (2 * Time.time);
		RightHand.localRotation = Quaternion.Euler(new Vector3(RightHand.localRotation.x, RightHand.localRotation.y, _rightHandStartRot - _rightHandChange));
	}
}
