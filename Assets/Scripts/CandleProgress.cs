﻿using UnityEngine;
using System.Collections;

public class CandleProgress : MonoBehaviour
{
	
	[Range(0.0f, 1.0f)]
	public float Value;
	
	SpriteRenderer FrameSprite;
	SpriteRenderer DarkSprite;
	SpriteRenderer ValueSprite;

	Candle candle;

	void Awake()
	{
		FrameSprite = transform.FindChild ("Frame").GetComponent<SpriteRenderer> ();
		DarkSprite = transform.FindChild ("Dark").GetComponent<SpriteRenderer> ();
		ValueSprite = transform.FindChild ("Value").GetComponent<SpriteRenderer> ();
		candle = GetComponentInParent<Candle> ();
		
		FrameSprite.enabled = false;
		DarkSprite.enabled = false;
		ValueSprite.enabled = false;
	}

	void Update ()
	{
		Vector3 scale = DarkSprite.transform.localScale;
		DarkSprite.transform.localScale = new Vector3 (1.0f - Value, scale.y, scale.z);

		bool show = candle.LitProgress > 0.01f;

		FrameSprite.enabled = DarkSprite.enabled = ValueSprite.enabled = show;
		Value = candle.LitProgress;
	}
}
