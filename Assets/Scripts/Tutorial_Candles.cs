﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Tutorial_Candles : MonoBehaviour
{

	public GameObject Dark;
	public GameObject LitHead;
	public Sprite Candle;
	public Sprite CandleExt;

	public Image CandleImage;
	public Slider CandleSlider;
	
	void Start ()
	{
		StartCoroutine (Animation ());
	}

	IEnumerator Animation()
	{
		while (true)
		{
			Dark.SetActive(true);
			LitHead.SetActive(false);
			CandleImage.sprite = CandleExt;
			CandleSlider.value = 0;
			for(int i=0; i<50; i++)
			{
				CandleSlider.value = i / 50.0f;
				yield return new WaitForFixedUpdate();
			}
			CandleSlider.value = 1.0f;

			Dark.SetActive(false);
			LitHead.SetActive(true);
			CandleImage.sprite = Candle;
			yield return new WaitForSeconds(1.0f);

		}
	}
}
