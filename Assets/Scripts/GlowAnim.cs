﻿using UnityEngine;
using System.Collections;

public class GlowAnim : MonoBehaviour
{

	public bool Visible;

	const float Speed = 3.0f;
	SpriteRenderer spriteRenderer;
	float _visibilityTimer;
	
	void Start ()
	{
		Visible = false;
		_visibilityTimer = 0;
		spriteRenderer = GetComponent<SpriteRenderer> ();
	}

	void Update ()
	{
		if (Visible)
			_visibilityTimer += Time.deltaTime * 4.0f;
		else
			_visibilityTimer -= Time.deltaTime * 4.0f;

		_visibilityTimer = Mathf.Clamp01 (_visibilityTimer);

		spriteRenderer.color = new Color(1, 1, 1, Mathf.Abs((Mathf.Sin(Time.realtimeSinceStartup*Speed)+0.2f)/1.2f)*_visibilityTimer);
	}
}
