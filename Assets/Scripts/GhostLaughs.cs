﻿using UnityEngine;
using System.Collections;

public class GhostLaughs : MonoBehaviour {

	AudioSource[] ghosts;
	float timer;
	float startTime;

	float getTimeInterval()
	{
		return Random.Range(10.0f, 20.0f);
	}

	// Use this for initialization
	void Start () {
		ghosts = GetComponentsInParent<AudioSource> ();
		startTime = Time.time;
		timer = getTimeInterval();
	}
	
	// Update is called once per frame
	void Update () {
		if (Time.time - startTime >= timer)
		{
			ghosts[Random.Range(0, ghosts.Length)].Play ();
			startTime = Time.time;
			timer = getTimeInterval();
		}
	}
}
