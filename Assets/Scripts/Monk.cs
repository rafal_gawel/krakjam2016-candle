﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum MonkAnimationState {
	IDLE, WALKING, LIGHTING
}

public class Monk : MonoBehaviour
{

	public static Monk Instance;

	public List<NavNode> Path {
		set
		{
			_path = value;
			_nextNode = 0;
		}
	}

	public float Speed;
	private bool gameOver;

	public Vector2 Direction { get; set; }

	public void GameOver ()
	{
		gameOver = true;
	}
	
	MonkAnimationState _currentAnimationState = MonkAnimationState.WALKING;
	public Transform Pivot;
	public bool Lighting;
	public SpriteRenderer Body;
	public SpriteRenderer Head;
	public SpriteRenderer CandleHand;
	public SpriteRenderer RightHand;
	Vector3 _startBodyPos;
	Vector3 _startHeadPos;
	Vector3 _startCandleHandPos;
	Vector3 _startRightHandPos;
	Quaternion _startCandleHandRot;
	Quaternion _startRightHandRot;
	List<NavNode> _path = null;
	int _nextNode;

	LTDescr LTHead;
	LTDescr LTCandleHand;
	LTDescr LTRightHand;
	
	void Awake()
	{
		Instance = this;
		gameOver = false;
	}

	void Start()
	{
		_startBodyPos = Body.transform.localPosition;
		_startHeadPos =Head.transform.localPosition;
		_startCandleHandPos =CandleHand.transform.localPosition;
		_startRightHandPos =RightHand.transform.localPosition;

		_startCandleHandRot =CandleHand.transform.localRotation;
		_startRightHandRot =RightHand.transform.localRotation;
	}

	void Update()
	{
		Vector3 direction = Vector3.zero;

		bool shouldRepeat = false;
		do {
			shouldRepeat = false;
			if(_path != null && _nextNode < _path.Count)
			{
				Vector3 dir3d = _path[_nextNode].transform.position - Pivot.transform.position;
				if(dir3d.magnitude < 0.1f)
				{
					_nextNode++;
					shouldRepeat = true;
				} else
				{
					direction = dir3d.normalized;
				}
			}
		} while(shouldRepeat);

		transform.position += direction * Time.deltaTime * Speed;

		if (Lighting)
		{
			SetAnimationState(MonkAnimationState.LIGHTING);
		}
		else if(direction.magnitude < 0.01f)
		{
			SetAnimationState(MonkAnimationState.IDLE);
		}
		else 
		{
			SetAnimationState(MonkAnimationState.WALKING);
		}

		if(Mathf.Abs(direction.x) > 0.01f)
		{
			transform.localScale = new Vector3(direction.x < 0 ? -1 : 1, 1, 1);
		}
	}

	public void SetAnimationState(MonkAnimationState _newAnimationState)
	{
		if (_newAnimationState != _currentAnimationState)
		{
			StopAnimation();
			_currentAnimationState = _newAnimationState;
			StartAnimation();
		}
	}

	void StopAnimation ()
	{
		if (LTHead != null) {
			Head.transform.localPosition = _startHeadPos;
			LeanTween.cancel(LTHead.uniqueId);
		}
		if (LTCandleHand != null)
		{
			CandleHand.transform.localPosition = _startCandleHandPos;
			LeanTween.cancel(LTCandleHand.uniqueId);
		}
		if (LTRightHand != null) {
			RightHand.transform.localPosition = _startRightHandPos;
			RightHand.transform.localRotation = _startRightHandRot;
			LeanTween.cancel(LTRightHand.uniqueId);
		}
	}
	
	void StartAnimation ()
	{
		if (_currentAnimationState == MonkAnimationState.IDLE)
		{
			LTCandleHand = LeanTween.rotateZ(CandleHand.gameObject, CandleHand.gameObject.transform.localRotation.z - 7.5f, 1f).setLoopPingPong();
			LTRightHand = LeanTween.rotateZ(RightHand.gameObject, RightHand.gameObject.transform.localRotation.z + 2.5f, 1f)
				.setEase(LeanTweenType.easeInOutSine)
				.setLoopPingPong();
		}
		else if (_currentAnimationState == MonkAnimationState.WALKING)
		{
			LTHead = LeanTween.moveLocalY(Head.gameObject, Head.gameObject.transform.localPosition.y - 0.02f, 1f).setLoopPingPong();
			LTCandleHand = LeanTween.moveLocalY(CandleHand.gameObject, CandleHand.gameObject.transform.localPosition.y + 0.06f, 1f).setLoopPingPong();
			LTRightHand = LeanTween.rotateZ(RightHand.gameObject, RightHand.gameObject.transform.localRotation.z + 10f, 0.5f)
				.setEase(LeanTweenType.easeInOutSine)
				.setLoopPingPong();
		}
		else if (_currentAnimationState == MonkAnimationState.LIGHTING)
		{
			LTCandleHand = LeanTween.rotateZ(CandleHand.gameObject, CandleHand.gameObject.transform.localRotation.z - 30f, 1f);
		}
	}
}