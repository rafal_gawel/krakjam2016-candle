﻿using System.Collections;
using UnityEngine;

public class CameraTool : MonoBehaviour
{
	public Vector3 Shift { get; set; }
	public static CameraTool Instance;
	
	Camera _camera;
	Vector3 _shakeShift;
	
	void Awake ()
	{
		_camera = GetComponent<Camera> ();
		Instance = this;
	}
	
	void Update()
	{
		transform.position = _shakeShift + Shift + new Vector3(0, 0, -10);
	}
	
	public void ShakeCamera(float duration, float force)
	{
		StartCoroutine(Shake (duration, force));
	}
	
	private IEnumerator Shake(float duration, float force)
	{
		var elapsed = 0.0f;
		
		while (elapsed < duration) 
		{
			elapsed += Time.deltaTime;          
			
			var percentComplete = elapsed / duration;         
			var damper = 1.0f - Mathf.Clamp(4.0f * percentComplete - 3.0f, 0.0f, 1.0f);
			
			// map value to [-1, 1]
			var x = Random.value * 2.0f - 1.0f;
			var y = Random.value * 2.0f - 1.0f;
			x *= force * damper;
			y *= force * damper;
			
			_shakeShift = new Vector3(x, y, 0);
			
			yield return null;
		}
		
		_shakeShift = Vector3.zero;
	}
	
}
