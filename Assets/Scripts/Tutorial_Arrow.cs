﻿using UnityEngine;
using System.Collections;

public class Tutorial_Arrow : MonoBehaviour
{
	public float Interval;
	public float Range;
	public float BeforeDelay;
	public float AfterDelay;

	void Start ()
	{
		StartCoroutine (Animation ());
	}

	IEnumerator Animation()
	{
		transform.localPosition = new Vector3(0, -Range, 0);
		while(true)
		{
			yield return new WaitForSeconds(BeforeDelay);
			LeanTween.moveLocalY(gameObject, Range, Interval).setEase(LeanTweenType.easeOutBounce);
			yield return new WaitForSeconds(Interval);
			yield return new WaitForSeconds(AfterDelay);
			LeanTween.moveLocalY(gameObject, -Range, Interval).setEase(LeanTweenType.easeInSine);
			yield return new WaitForSeconds(Interval);
		}
	}

}
