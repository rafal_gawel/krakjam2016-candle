﻿using UnityEngine;
using System.Collections;

public class Tutorial_HourGlass : MonoBehaviour
{

	public float Interval;
	public float AnimTime;

	void Start ()
	{
		StartCoroutine (Animation ());
	}

	IEnumerator Animation()
	{
		while(true)
		{
			transform.rotation = Quaternion.identity;

			yield return new WaitForSeconds(Interval);

			LeanTween.rotateZ(gameObject, 180, AnimTime).setEase(LeanTweenType.easeInOutBack);

			yield return new WaitForSeconds(AnimTime);

			yield return new WaitForSeconds(Interval);
			
			LeanTween.rotateZ(gameObject, 360, AnimTime).setEase(LeanTweenType.easeInOutBack);
			
			yield return new WaitForSeconds(AnimTime);
		}
	}

}
