﻿using UnityEngine;
using System.Collections;

public class Ghost : MonoBehaviour {
    private Vector3 targetPosition;
    private Vector3 lastPosition;
	private State state = State.Going;
	private float timer;
	private float driftTimer;
	private Vector3 oldDriftDir;
	private Vector3 newDriftDir;
	private Candle targetCandle;
	private int candlesLeft;
	private bool exorcised;
	private float speed;
	private int[] minCandles;
	private int[] maxCandles;


	[HideInInspector]
	public float TimeAboveCandle = 1f; // TODO Move to BalanceManager

	enum State
	{
		Going,
		Capturing
	}

    Vector3 getRandomPosBeyondMap()
    {
        if (Random.Range(0, 2) % 2 == 1)
        {
            float startX = Random.Range(0, 2) % 2 == 1 ? -8.2f : 8.2f;
            float startY = Random.Range(-6.5f, 5.5f);
            return new Vector3(startX, startY, 0.0f);
        }
        else
        {
            float startX = Random.Range(-8.2f, 8.2f);
            float startY = Random.Range(0, 2) % 2 == 1 ? -6.5f : 5.5f;
            return new Vector3(startX, startY, 0.0f);
        }
    }

	void setupGoing()
	{
		lastPosition = transform.position;
		if (candlesLeft > 0 && !exorcised)
		{
			var candles = FindObjectsOfType<Candle>();
			int candleIndex;
			do
			{
				candleIndex = Random.Range(0, candles.Length);
			}
			while (targetCandle == candles[candleIndex]);
			targetCandle = candles[candleIndex];
			targetPosition = targetCandle.transform.position;
		}
		else
		{
			targetPosition = getRandomPosBeyondMap();
		}
		oldDriftDir = new Vector3 (Random.Range (-1.0f, 1.0f), Random.Range (-1.0f, 1.0f), 0.0f).normalized;
		newDriftDir = new Vector3 (Random.Range (-1.0f, 1.0f), Random.Range (-1.0f, 1.0f), 0.0f).normalized;
		driftTimer = Time.time;
	}

	public void GoAway ()
	{
		exorcised = true;
		setupGoing ();
	}

    void Start ()
	{
		var ghostSpawner = FindObjectOfType<GhostSpawner> ();
		speed = ghostSpawner.ghostSpeed;
		minCandles = ghostSpawner.minCandles;
		maxCandles = ghostSpawner.maxCandles;

		candlesLeft = Random.Range(minCandles[FindObjectOfType<BalanceManager>().getLevel()],
		                           maxCandles[FindObjectOfType<BalanceManager>().getLevel()] + 1);
        transform.position = getRandomPosBeyondMap();
		setupGoing();
		exorcised = false;
    }
	
	void FixedUpdate ()
	{
		switch (state)
		{
			case State.Going:
				var dir = (targetPosition - transform.position).normalized;
				if (Time.time - driftTimer >= 1.0f)
				{
					oldDriftDir = newDriftDir;
					newDriftDir = new Vector3 (Random.Range (-1.0f, 1.0f), Random.Range (-1.0f, 1.0f), 0.0f).normalized;
					driftTimer = Time.time;
				}
				var driftDir = Vector3.Lerp(oldDriftDir, newDriftDir, Time.time - driftTimer);
				driftDir = driftDir.normalized * Mathf.Min((targetPosition - transform.position).magnitude, 1.0f);
				dir += driftDir * 0.6f;

				if (Mathf.Abs(dir.x) > 0.01f)
				{
					var scale = transform.localScale;
					scale.x = (dir.x < 0 ? -1 : 1) * Mathf.Abs(scale.x);
					transform.localScale = scale;
				}

				var slowDown1 = Mathf.Min((targetPosition - transform.position).magnitude, 1.0f);
				var slowDown2 = Mathf.Min((transform.position - lastPosition).magnitude, 1.0f) + 0.1f;
				float actualSpeed = exorcised ? 0.075f : speed;
				transform.position += dir.normalized * actualSpeed * Mathf.Min(slowDown1, slowDown2);
				if ((targetPosition - transform.position).magnitude <= 0.1f)
				{
					if (candlesLeft > 0 && !exorcised)
					{
						if (targetCandle.Lit)
						{
							GetComponent<AudioSource>().Play();
							state = State.Capturing;
							timer = Time.time;
						}
						--candlesLeft;
						setupGoing();
					}
					else
					{
						Destroy(gameObject);
					}
				}
				break;
			case State.Capturing:
				if (Time.time - timer >= TimeAboveCandle)
				{
					state = State.Going;
					setupGoing();
				}
				break;
		}
	}
}
