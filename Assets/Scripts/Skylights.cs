﻿using UnityEngine;
using System.Collections;

public class Skylights : MonoBehaviour
{
	static bool created = false;
	
	void Start ()
	{
		if(created)
		{
			GameObject.Destroy(gameObject);
			return;
		} else
		{
			created = true;
			GameObject.DontDestroyOnLoad(gameObject);
		}
	}
}
