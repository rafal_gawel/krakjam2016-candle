﻿using UnityEngine;
using System.Collections;

public class Fog : MonoBehaviour
{

	public GameObject Layer1;
	public GameObject Layer2;

	static bool created = false;
	
	void Start ()
	{
		if(created)
		{
			GameObject.Destroy(gameObject);
			return;
		} else
		{
			created = true;
			GameObject.DontDestroyOnLoad(gameObject);
		}
		StartCoroutine (Routine ());
	}

	IEnumerator Routine()
	{
		Vector3[] layerPositions = new Vector3[] {
			new Vector3(14, 14),
			new Vector3(-14, 14),
			new Vector3(14, -14),
			new Vector3(-14, -14)
		};

		const float time = 40.0f;

		while(true)
		{
			for(int i=0; i<4; i++)
			{
				int j=(i+1)%4;
				LeanTween.move(Layer1, layerPositions[i], time).setEase(LeanTweenType.easeInOutSine);
				LeanTween.move(Layer2, layerPositions[j], time).setEase(LeanTweenType.easeInOutSine);
				yield return new WaitForSeconds(time);
			}
		}
	}

}
