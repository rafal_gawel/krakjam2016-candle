﻿using UnityEngine;
using System.Collections;

public class Splash : MonoBehaviour
{

	public AudioClip ThunderAudio;

	public GameObject Title;
	public GameObject Background;
	public GameObject Lightning;
	public CameraTool CameraTool;

	void Start()
	{
		LeanTween.scale (Title, Vector3.one, 2.0f).setEase(LeanTweenType.easeInOutSine);
		LeanTween.scale (Background, Vector3.one * 1.3f, 2.0f).setEase(LeanTweenType.easeInOutSine);
		Lightning.SetActive (false);

		StartCoroutine (Routine ());
	}

	IEnumerator Routine()
	{
		yield return new WaitForSeconds(2.0f);

		Lightning.SetActive (true);
		PlayThunder ();
		yield return new WaitForSeconds(0.05f);
		Lightning.SetActive (false);
		CameraTool.ShakeCamera (0.6f, 0.15f);
		yield return new WaitForSeconds(0.1f);
		PlayThunder ();
		Lightning.SetActive (true);
		yield return new WaitForSeconds(0.05f);
		Lightning.SetActive (false);
		yield return new WaitForSeconds (2.3f);
		Application.LoadLevel ("Start");
	}

	void PlayThunder()
	{
		GetComponent<AudioSource> ().PlayOneShot (ThunderAudio);
	}

	void Update ()
	{
	
	}
}
