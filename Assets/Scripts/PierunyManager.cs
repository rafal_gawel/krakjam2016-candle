﻿using UnityEngine;
using System.Collections;

public class PierunyManager : MonoBehaviour {

	public Sprite[] Pieruny;
	public GameObject PierunAnimationPrefab;
	
	public static PierunyManager Instance;
	
	void Awake ()
	{
		Instance = this;
	}

	public void ShowPierun(Vector3 aimPosition)
	{
		GameObject go = (GameObject) Object.Instantiate(PierunAnimationPrefab, aimPosition, Quaternion.identity);
		go.GetComponent<PierunAnimation>().ShowPierun(Pieruny[Random.Range (0, Pieruny.Length)]);
	}
}