﻿using UnityEngine;
using System.Collections;

public class CameraScaler : MonoBehaviour
{
	void Update ()
	{
		float aspect = Camera.main.aspect;
		// width = 1500 height = 1000 aspect = 0.6666
		const float baseAspect = 3.0f / 2.0f;

		if (aspect >= baseAspect)
			Camera.main.orthographicSize = 5.0f;
		else
			Camera.main.orthographicSize = 7.5f / aspect;
	}
}
