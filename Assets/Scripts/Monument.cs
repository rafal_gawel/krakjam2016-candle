﻿using UnityEngine;
using System.Collections;

public class Monument : MonoBehaviour {
	public GameObject gargoyleLitHead;
	public GameObject gargoyleDark;
	public GameObject blue;
	public GameObject glow;

	GlowAnim _glowAnim;

	float _minExorcisedPercentage = 0.5f; // TODO Move to BalanceManager
	public float MinExorcisedPercentage {
		get {
			return _minExorcisedPercentage;
		}
		set {
			_minExorcisedPercentage = value;
		}
	}

	public bool Exorcised {
		get;
		set;
	}
	bool _active;
	public bool Active {
		get {
			return _active;
		}
		set {
			if (value && PierunyManager.Instance != null)
			{
				PierunyManager.Instance.ShowPierun(transform.position);
			}
			_active = value;
		}
	}

	public bool AllCandlesLights {
		get {
			return _candlesLitCount == _candleChildren.Length;
		}
	}

	Candle[] _candleChildren;
	float _percentage;
	int _candlesLitCount;

	void Awake()
	{
		_candleChildren = GetComponentsInChildren<Candle>();
		_glowAnim = glow.GetComponent<GlowAnim> ();
	}

	void Start () {
		gargoyleLitHead.SetActive(false);
		_glowAnim.Visible = false;
	}
	
	void Update () {
		gargoyleLitHead.SetActive (Exorcised);
		_glowAnim.Visible = Active;
	}

	public void CandleUpdate (bool lit)
	{
		_candlesLitCount = lit ? _candlesLitCount + 1 : _candlesLitCount - 1;
		_percentage = _candlesLitCount / (float)_candleChildren.Length;
		var scale = gargoyleDark.transform.localScale;
		scale.y = 1.0f - Mathf.Clamp01(_candlesLitCount/((float)_minExorcisedPercentage*_candleChildren.Length));
		gargoyleDark.transform.localScale = scale;
		if (_percentage >= MinExorcisedPercentage - 0.01f)
		{
			if (!Exorcised)
			{
				GetComponent<AudioSource>().Play ();
			}
			Exorcised = true;
		}
		else
		{
			Exorcised = false;
		}
	}
}