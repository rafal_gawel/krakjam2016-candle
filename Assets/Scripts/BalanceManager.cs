﻿using UnityEngine;
using System.Collections;

public class BalanceManager : MonoBehaviour {
	public float[] thresholds;

	private float levelStartTime;

	public int getLevel()
	{
		float time = Time.time - levelStartTime;
		int i;
		for (i = 0 ; i < thresholds.Length ; ++i)
		{
			time -= thresholds[i];
			if (time <= 0.0f)
			{
				break;
			}
		}
		return i;
	}

	// Use this for initialization
	void Awake () {
		levelStartTime = Time.time;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
