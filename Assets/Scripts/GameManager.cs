﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour
{

	public static GameManager Instance;

	public enum GameState
	{
		Playing,
		Win,
		Lose
	}

	public bool IsPlaying {
		get {
			return gameState == GameState.Playing;
		}
	}

	public bool Won
	{
		get {
			return gameState == GameState.Win;
		}
	}

	public GameObject GameOverPanel;
	public GameObject WinPanel;

	GameState gameState = GameState.Playing;
	Monument[] monuments;

	void Awake()
	{
		Instance = this;
		monuments = FindObjectsOfType<Monument>();
	}

	void Start ()
	{
		GameOverPanel.SetActive (false);
		WinPanel.SetActive (false);
	}

	public void GameOver()
	{
		if(gameState == GameState.Playing)
		{
			gameState = GameState.Lose;
			GameOverPanel.SetActive(true);
		}
	}

	public void Win()
	{
		if(gameState == GameState.Playing)
		{
			gameState = GameState.Win;
			WinPanel.SetActive(true);
			var ghosts = FindObjectsOfType<Ghost>();
			foreach (var ghost in ghosts)
			{
				ghost.GoAway();
			}
			FindObjectOfType<GhostSpawner>().gameObject.SetActive (false);
			FindObjectOfType<GhostLaughs>().gameObject.SetActive (false);
			FindObjectOfType<Monk>().GameOver();
		}
	}

	void Update ()
	{
		if(AreAllExorcised())
			Win ();
	}

	public bool AreAllExorcised ()
	{
		bool result = true;
		for(int i=0; i<monuments.Length; i++)
		{
			if(!monuments[i].Exorcised)
				result = false;
		}
		return result;
	}

	public bool AreAllActive ()
	{
		bool result = true;
		for(int i=0; i<monuments.Length; i++)
		{
			if(!monuments[i].Active)
				result = false;
		}
		return result;
	}

}
