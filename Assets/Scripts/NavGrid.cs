﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class NavGrid : MonoBehaviour
{

	NavNode[] nodes;

	void Start ()
	{
		nodes = GetComponentsInChildren<NavNode>();

		for(int i=0; i<nodes.Length; i++)
		{
			nodes[i].NavGrid = this;
			nodes[i].Neighbors = GetNeighborsForNode(nodes[i]);
		}
	}

	NavNode[] GetNeighborsForNode(NavNode node)
	{
		int neighborsCount = 0;

		for(int i=0; i<nodes.Length; i++)
		{
			if(AreNeighbors(node, nodes[i]))
				neighborsCount++;
		}

		NavNode[] neighbors = new NavNode[neighborsCount];
		int currentNeighbor = 0;

		for(int i=0; i<nodes.Length; i++)
		{
			if(AreNeighbors(node, nodes[i]))
				neighbors[currentNeighbor++] = nodes[i];
		}

		return neighbors;
	}

	bool AreNeighbors(NavNode nodeA, NavNode nodeB)
	{
		if(nodeA == nodeB)
			return false;

		return Distance (nodeA, nodeB) < 1.5f;
	}

	float Distance(NavNode nodeA, NavNode nodeB)
	{
		return (nodeA.transform.position - nodeB.transform.position).magnitude;
	}

	public List<NavNode> GetPath(NavNode startNode, NavNode endNode)
	{
		const float infinity = 100000.0f;
		for(int i=0; i<nodes.Length; i++)
		{
			nodes[i].NextNode = null;
			nodes[i].Distance = infinity;
		}

		endNode.Distance = 0;

		Heap<NavNode> Q = new Heap<NavNode>(HeapType.MinHeap);
		for(int i=0; i<nodes.Length; i++)
			Q.Insert(nodes[i]);

		while(!Q.Empty)
		{
			NavNode u = Q.PopRoot();
			for(int i=0; i<u.Neighbors.Length; i++)
			{
				NavNode v = u.Neighbors[i];
				if(v.Distance > u.Distance + Distance(u, v))
				{
					v.Distance = u.Distance + Distance(u, v);
					v.NextNode = u;
					Q.Insert(v);
				}
			}
		}

		List<NavNode> path = new List<NavNode>();

		if(startNode.NextNode == null)
			return path;

		NavNode currentNode = startNode;
		path.Add(currentNode);

		while(currentNode != endNode)
		{
			currentNode = currentNode.NextNode;
			path.Add(currentNode);
		}

		return path;
	}

	public NavNode GetNearestNode(Vector3 position)
	{
		NavNode nearestNode = nodes[0];
		float minDistance = (nodes[0].transform.position-position).magnitude;

		for(int i=1; i<nodes.Length; i++)
		{
			float currentDistance = (nodes[i].transform.position-position).magnitude;
			if(currentDistance < minDistance)
			{
				minDistance = currentDistance;
				nearestNode = nodes[i];
			}
		}

		return nearestNode;
	}

	void Update ()
	{
	
	}
}
