﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class NavNode : MonoBehaviour, IComparable<NavNode>
{
	
	public NavGrid NavGrid { get; set; }
	public float Distance { get; set; }
	public NavNode[] Neighbors { get; set; }
	public NavNode NextNode { get; set; }

	#region IComparable implementation

	public int CompareTo (NavNode other)
	{
		return this.Distance.CompareTo(other.Distance);
	}

	#endregion

	void OnMouseDown()
	{
		NavNode startNode = NavGrid.GetNearestNode(Monk.Instance.Pivot.transform.position);
		List<NavNode> path = NavGrid.GetPath(startNode, this);
		for(int i=0; i<path.Count; i++)
		{
			Debug.Log ("NODE "+i, path[i]);
		}
		Monk.Instance.Path = path;
	}
	
}
