﻿using UnityEngine;
using System.Collections;

public class BumpAnim : MonoBehaviour
{
	
	public float Speed;

	void Update ()
	{
		float Range = 0.03f;
		float v = Mathf.Sin (Time.realtimeSinceStartup * Speed) * Range;
		transform.localScale = new Vector3 (1 + v, 1 - v, 1);
	}
}
