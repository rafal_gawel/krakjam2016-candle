﻿using UnityEngine;
using System.Collections;

public class MobileMovement : MonoBehaviour
{

	SpriteRenderer spriteRenderer;
	Vector3 dragStartPosition;
	bool moving = false;

	void Awake()
	{
		spriteRenderer = GetComponent<SpriteRenderer> ();
		spriteRenderer.enabled = false;
	}

	void Update ()
	{
		/*

		if(Input.GetMouseButtonDown(0))
		{
			spriteRenderer.enabled = true;
			dragStartPosition = MousePosition;
			moving = true;
			transform.position = dragStartPosition;
		}

		if(Input.GetMouseButtonUp(0))
		{
			spriteRenderer.enabled = false;
			moving = false;
		}

		if(moving)
		{
			Monk.Instance.Direction = MousePosition - dragStartPosition;
		} else
		{
			Monk.Instance.Direction = Vector2.zero;
		}
		*/
	}

	Vector3 MousePosition {
		get {
			Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
			mousePos.z = 0;
			return mousePos;
		}
	}
}
