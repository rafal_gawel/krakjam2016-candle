﻿using UnityEngine;
using System.Collections;

public class GhostFemaleAnimation : MonoBehaviour {

	public Transform LeftHand;
	public Transform RightHand;
	public Transform Head;

	float _leftHandStartRot;
	float _rightHandStartRot;
	float _headStartRot;
	float _leftHandChange;
	float _rightHandChange;
	float _headChange;

	const int handsAnimAnglesDegree = 25;
	const int headAnimAnglesDegree = 7;

	void Start () {
		_leftHandStartRot = -Quaternion.Angle(Quaternion.identity, LeftHand.localRotation); // It's stupid but it works
		_rightHandStartRot = Quaternion.Angle(Quaternion.identity, RightHand.localRotation); // It's stupid but it works
		_headStartRot = Quaternion.Angle(Quaternion.identity, Head.localRotation); // It's stupid but it works
//		Debug.Log ("_leftHandStartRot" + _leftHandStartRot, gameObject);
//		Debug.Log ("_rightHandStartRot" + _rightHandStartRot, gameObject);
	}
	
	void Update () {
		_leftHandChange = handsAnimAnglesDegree * Mathf.Sin (2 * Time.time);
		LeftHand.localRotation = Quaternion.Euler(new Vector3(LeftHand.localRotation.x, LeftHand.localRotation.y, _leftHandStartRot + _leftHandChange));

		_rightHandChange = handsAnimAnglesDegree * Mathf.Sin (2 * Time.time);
		RightHand.localRotation = Quaternion.Euler(new Vector3(RightHand.localRotation.x, RightHand.localRotation.y, _rightHandStartRot - _rightHandChange));

		_headChange = headAnimAnglesDegree * Mathf.Sin (Time.time);
		Head.localRotation = Quaternion.Euler(new Vector3(Head.localRotation.x, Head.localRotation.y, _headStartRot + _headChange));
	}
}
