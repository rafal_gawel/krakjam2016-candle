﻿using UnityEngine;
using System.Collections;

public class Tutorial_GhostSway : MonoBehaviour
{
	public float RangeX;
	public float RangeY;
	public float SpeedX;
	public float SpeedY;

	Vector3 startPosition;
	RectTransform rectTransform;

	void Start ()
	{
		rectTransform = GetComponent<RectTransform> ();
		startPosition = rectTransform.anchoredPosition;
	}

	void Update ()
	{
		rectTransform.anchoredPosition = startPosition + new Vector3 (Mathf.Sin(Time.realtimeSinceStartup*SpeedX)*RangeX, Mathf.Sin (Time.realtimeSinceStartup * SpeedY) * RangeY, 0);
	}
}
