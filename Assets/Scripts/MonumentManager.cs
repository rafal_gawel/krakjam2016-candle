using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MonumentManager : MonoBehaviour {

	public static MonumentManager Instance;

//	float _intervalTimeRandomRange = 2f;
	public float _turnTime = 7f; // TODO Move to BalanceManager
	public float IntervalTime {
		get {
			return _turnTime; //+ Random.Range(-_intervalTimeRandomRange, _intervalTimeRandomRange);
		}
		set {_turnTime = value;}
	}
	int MinActiveCountInTurn = 1; // TODO Move to BalanceManager
	int MaxActiveCountInTurn = 2; // TODO Move to BalanceManager

	Monument[] _monumentReferences;

	bool _skipTurn;

	void Awake () {
		Instance = this;
	}

	void Start()
	{
		_monumentReferences = FindObjectsOfType<Monument>();
		NextTurn();
	}

	void NextTurn ()
	{	
		_skipTurn = false;

		int nextActiveCount = Random.Range (MinActiveCountInTurn, MaxActiveCountInTurn + 1);
		for (int i = 0; i < nextActiveCount; i++)
		{
			StartCoroutine(MonumentActivationProcess());
		}

		StartCoroutine (NextTurnTimer(_turnTime));
	}

	IEnumerator MonumentActivationProcess ()
	{
		if(!GameManager.Instance.AreAllExorcised() && !GameManager.Instance.AreAllActive())
		{
			int randomNumber = Random.Range(0, _monumentReferences.Length);
			int monumentIndex = -1;
			for(int i=0; i<_monumentReferences.Length; i++)
			{
				int possibleOkIndex = (randomNumber + i) % _monumentReferences.Length;
                if (!_monumentReferences[possibleOkIndex].Active && !_monumentReferences[possibleOkIndex].Exorcised)
                {
					monumentIndex = possibleOkIndex;
					break;
				}
			}
			if (monumentIndex != -1)
			{
				_monumentReferences[monumentIndex].Active = true;

				for(float t=0; t<IntervalTime && !_skipTurn; t+=0.1f)
					yield return new WaitForSeconds(0.1f);

				_monumentReferences[monumentIndex].Active = false;
			}
		}
		yield break;
	}

	IEnumerator NextTurnTimer (float timeToNextTurn)
	{
		for(float t=0; t<timeToNextTurn; t+=0.1f)
		{
			yield return new WaitForSeconds(0.1f);
			if(ShouldSkipTurn())
			{
				_skipTurn = true;
				break;
			}
		}
		if(_skipTurn)
			yield return new WaitForSeconds (0.2f);
		NextTurn();
	}

	bool ShouldSkipTurn()
	{
		bool allActiveCandlesLights = true;
		for(int i=0; i<_monumentReferences.Length; i++)
		{
			if(_monumentReferences[i].Active)
			{
				if(!_monumentReferences[i].AllCandlesLights)
					allActiveCandlesLights = false;
			}
		}

		return allActiveCandlesLights;
	}

	void Update () {
	
	}

}