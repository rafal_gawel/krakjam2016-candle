﻿using UnityEngine;
using System.Collections;

public class GhostSpawner : MonoBehaviour {
	private float timer;
	private Vector2 spawnIntervalVec;
	private float spawnInterval;

	public int maxGhostCountOnMap = 6;
	public float ghostSpeed = 0.05f;
	public GameObject[] ghostPrefabs;
	public int[] ghostCounts;
	public Vector2[] spawnIntervals;
	public int[] minCandles;
	public int[] maxCandles;

	void spawnGhosts()
	{
		int ghostCount = ghostCounts[FindObjectOfType<BalanceManager>().getLevel()];
		for (int i = 0 ; i < ghostCount ; ++i)
		{
			if (FindObjectsOfType<Ghost>().Length < maxGhostCountOnMap)
			{
				Instantiate(ghostPrefabs[Random.Range (0, ghostPrefabs.Length)]);
			}
		}
	}

	void setTimer()
	{
		timer = Time.time;
		spawnIntervalVec = spawnIntervals[FindObjectOfType<BalanceManager>().getLevel()];
		spawnInterval = Random.Range(spawnIntervalVec.x, spawnIntervalVec.y);
	}

	// Use this for initialization
	void Start () {
		spawnGhosts();
		setTimer();
	}
	
	// Update is called once per frame
	void Update () {
		if (Time.time - timer >= spawnInterval)
		{
			spawnGhosts();
			setTimer();
		}
	}
}
