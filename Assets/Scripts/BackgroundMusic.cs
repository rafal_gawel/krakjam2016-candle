﻿using UnityEngine;
using System.Collections;

public class BackgroundMusic : MonoBehaviour
{

	static bool created = false;

	void Awake ()
	{
		if(created)
		{
			GameObject.Destroy(gameObject);
		} else
		{
			created = true;
			GameObject.DontDestroyOnLoad(gameObject);
		}
	}

}
