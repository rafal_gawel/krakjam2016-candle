﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Hourglass : MonoBehaviour
{
	public RectTransform UpSand;
	public RectTransform DownSand;
	public RectTransform MiddleSand;

	public float TimeToGameOver;

	private AudioSource clock4;
	private AudioSource clockFinal;
	
	float Value;
	bool playing4;
	bool playingFinal;

	Vector2 _upSandSizeDelta;
	Vector2 _downSandSizeDelta;

	void Start ()
	{
		_upSandSizeDelta = UpSand.sizeDelta;
		_downSandSizeDelta = DownSand.sizeDelta;
		Value = 1.0f;
		clock4 = GetComponents<AudioSource> () [0];
		clockFinal = GetComponents<AudioSource> () [1];
		playing4 = false;
		playingFinal = false;
		if (Camera.main.gameObject.GetComponent<CameraTool> () == null)
			Camera.main.gameObject.AddComponent<CameraTool> ();
	}

	void Update ()
	{
		if (!GameManager.Instance.IsPlaying)
			return;

		float sandProgress = Mathf.Clamp01 (Value);

		UpSand.sizeDelta = _upSandSizeDelta * sandProgress;
		DownSand.sizeDelta = _downSandSizeDelta * (1-sandProgress);

		MiddleSand.gameObject.SetActive (!(Value >= 0.99f || Value <= 0.01f));

		Value -= Time.deltaTime / TimeToGameOver;

		if (Value < 10.5f / TimeToGameOver && !playing4)
		{
			playing4 = true;
			clock4.Play ();
			StartCoroutine(ShakeAnimation());
		}
		else if (Value < 0)
		{
			if (!playingFinal)
			{
				playingFinal = true;
				clockFinal.Play ();
			}
			GameManager.Instance.GameOver();
		}
	}

	IEnumerator ShakeAnimation()
	{
		yield return new WaitForSeconds (0.1f);
		for(int i=0; i<4; i++)
		{
			if(GameManager.Instance.IsPlaying)
				CameraTool.Instance.ShakeCamera (.15f, 0.1f);
			yield return new WaitForSeconds(2.55f);
		}
	}
}
